#!/usr/bin/env python

import cdsapi
import argparse
import os
import subprocess

c = cdsapi.Client()

parser = argparse.ArgumentParser(description='Retrieve a full year of ERA5 hourly t2m')
parser.add_argument("year", help="Year to retrieve")
args = parser.parse_args()
year = args.year

topdir = '/net/pc170547/nobackup_1/users/sager/CX/TMP'
target = os.path.join(topdir,"era5_t2m_{}".format(year))

print('Start retrieving {}'.format(target))

if not os.path.exists(target):
    c.retrieve(
        'reanalysis-era5-single-levels',
        {
            'product_type': 'reanalysis',
            'format': 'grib',
            'variable': '2m_temperature',
            'year': year,
            'month': [
                '01', '02', '03',
                '04', '05', '06',
                '07', '08', '09',
                '10', '11', '12',
            ],
            'day': [
                '01', '02', '03',
                '04', '05', '06',
                '07', '08', '09',
                '10', '11', '12',
                '13', '14', '15',
                '16', '17', '18',
                '19', '20', '21',
                '22', '23', '24',
                '25', '26', '27',
                '28', '29', '30',
                '31',
            ],
            'time': [
                '00:00', '01:00', '02:00',
                '03:00', '04:00', '05:00',
                '06:00', '07:00', '08:00',
                '09:00', '10:00', '11:00',
                '12:00', '13:00', '14:00',
                '15:00', '16:00', '17:00',
                '18:00', '19:00', '20:00',
                '21:00', '22:00', '23:00',
            ],
        }, target)

print('Done retrieving {}'.format(target))


# Format to mimic era5_request_daily_cds.py retrieval:  monthly files named era5_daily_YYYYMM__oper_an_sfc.grb
#
# At the CLI
# export CDO_FILE_SUFFIX=__oper_an_sfc.grb ; cdo splitmon era5-2021-t2m.grb era5_daily_2021

os.environ['CDO_FILE_SUFFIX'] = '__oper_an_sfc.grb'
prefix = os.path.join(topdir,'era5_daily_{}'.format(year))
cmd = 'cdo splitmon {} {}'.format(target, prefix)
p = subprocess.Popen( cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
stdout_str, stderr_str = p.communicate()

if p.returncode:
    print(stdout_str, stderr_str)
    raise RuntimeError('\n\terror with command: {}'.format(cmd))
else:
    print( 'Done month splitting {}'.format(year))
    os.remove(target)
