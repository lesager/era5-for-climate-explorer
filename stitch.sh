#! /usr/bin/env bash

set -e

###############################
# stich and send to CX server #
###############################

finaldst='/nobackup_2/users/sager/ERA5' # dir with final product

usage()
{
    echo "Usage: ${0##*/} [-d] [-m] [-x] [-s] "
    echo ""
    echo "   -d : concatenate daily data [now only TMIN and TMAX to compute their monthly mean with -x]"
    echo "                               [for other vars GJ remap to 0.5 global, and 0.25 Europe only ]"
    echo "   -m : concatenate monthly data"
    echo "   -x : extra data, i.e. monthly mean of daily Tmax and Tmin"
    echo 
    echo "  PRODUCTION:  ${0##*/} -dmx"
}

# -- default options (do nothing by default)
do_daily=0
do_monthly=0
do_extra=0
do_sent=0

while getopts "h?dmxs" opt; do
    case "$opt" in
        h|\?)
            usage
            exit 0
            ;;
        d)  do_daily=1 ;;
        m)  do_monthly=1 ;;
        x)  do_extra=1 ;;
        s)  do_sent=1 ;;
    esac
done
shift $((OPTIND-1))


# ----- UTILS
is_leap_year() {                ## USAGE: is_leap_year [year]
    case $1 in
        *0[48] |\
        *[2468][048] |\
        *[13579][26] |\
        *[13579][26]0|\
        *[2468][048]00 |\
        *[13579][26]00 ) _IS_LEAP_YEAR=1
            return 0 ;;
        *) _IS_LEAP_YEAR=0
            return 1 ;;
    esac
}

last_day() {                         ## Return number of days in YYYYMM
    local year day
    year=${1:0:4}
    day=${1:4}
    case $day in
        09|04|06|11)
            dim_d=30 ;;
        01|03|05|07|08|10|12)
            dim_d=31 ;;
        02)
            is_leap_year $year &&
                dim_d=29 ||
                    dim_d=28 ;;
    esac
    echo ${dim_d}
}

ofix() {
    # -- GJ extra metadata (more or less hardcoded, since I cannot
    # -- call his scripts w/o crashing - too much dependencies)

    # Assumption :
    #     $yyyymmS and $yyyymmE are defined
    # Inputs:
    #     $1: filename to process
    #     $2: climex field
    #     $3: 25 or 28 (for a 0.25 or 0.28 resolution)

    [[ $3 -eq 28 ]] && latrange='89.78' || latrange='90.'
    [[ $3 -eq 28 ]] && minlon='-0.14' || minlon='-0.12'
    [[ $3 -eq 28 ]] && maxlon='359.86' || maxlon='359.88'
    
    ncatted -h -a geospatial_lat_min,global,o,f,-${latrange} $1
    ncatted -h -a geospatial_lat_max,global,o,f,${latrange} $1
    ncatted -h -a geospatial_lat_units,global,o,c,"degrees_north" $1

    ncatted -h -a geospatial_lon_min,global,o,f,${minlon} $1
    ncatted -h -a geospatial_lon_max,global,o,f,${maxlon} $1
    ncatted -h -a geospatial_lon_units,global,o,c,"degrees_east" $1
    ncatted -h -a geospatial_lon_resolution,global,o,f,0.$3 $1

    adate=${yyyymmS:0:4}-${yyyymmS:4}-01
    edate=${yyyymmE:0:4}-${yyyymmE:4}-$(last_day ${yyyymmE})

    ncatted -h -a time_coverage_start,global,c,c,$adate $1
    ncatted -h -a time_coverage_stop,global,c,c,$edate $1
    ncatted -h -a time_coverage_end,global,c,c,$edate $1

    climexp_url="https://climexp.knmi.nl/select.cgi?$2"
    ncatted -h -a climexp_url,global,o,c,$climexp_url $1
}

# ----- start processing
t1=$(date +%s)

cd $finaldst/
mkdir -p $finaldst/final

# ----- DAILY
if (( do_daily ))
then
    #vars=(t2m tdew msl sp z500 t500 q500 sfcWind sfcWindmax tmin tmax tp evap)
    vars=(tmin tmax)

    # limit number of parallel processings to 4
    for j in {0..3}
    do
        is=$((j*4))
        for v in ${vars[*]:${is}:4}
        do
            ( echo "merging daily $v"
              files=$(find . -type f -name "era5_??????_${v}.nc" | sort )

              yyyymmS=$(echo $files | awk '{print $1}'  | sed "s|.*era5_\(......\)_.*\.nc|\1|")
              yyyymmE=$(echo $files | awk '{print $NF}' | sed "s|.*era5_\(......\)_.*\.nc|\1|")
          
              if [[ -n $files ]]
              then
                  cdo -O -z zip mergetime $files final/era5_${v}_daily.nc

                  #OBSOLETE # re-apply daily operator for forecast variables to account for overlapping year
                  #OBSOLETE dayop=
                  #OBSOLETE case ${v} in
                  #OBSOLETE     tp|evap) dayop=daysum ;;
                  #OBSOLETE     tmin) dayop=daymin ;;
                  #OBSOLETE     tmax) dayop=daymax ;;
                  #OBSOLETE esac
                  #OBSOLETE if [ -n ${dayop} ]
                  #OBSOLETE then
                  #OBSOLETE     cdo -f nc4 -z zip $dayop final/era5_${v}_daily.nc ${v}-temp.nc
                  #OBSOLETE     mv ${v}-temp.nc final/era5_${v}_daily.nc
                  #OBSOLETE fi
                  
                  ofix final/era5_${v}_daily.nc era5_${v}_daily 25
              else
                  echo "no daily files to merge for $v"
              fi
            ) &
            pid0=$!
            pids="$pids $pid0"
            vid[$pid0]=$v    
        done

        RESULT=
        for pid in $pids; do
            wait $pid || RESULT="$RESULT $pid"
        done

        if [ -n "$RESULT" ]
        then
            for errcode in $RESULT
            do
                echo "ERROR with monthly merging of ${vid[$errcode]}"
            done
            exit 1
        fi
        pids=
        unset -v vid
    done
fi

# ----- MONTHLY
if (( do_monthly ))
then
    vars="z850 z700 z500 z300 z200 wspd w850 w700 w500 w300 w200 vap v850 v700 v500 v300 v200 v10 \
uzon u850 u700 u500 u300 u200 u10 tzon trbflx tp t850 t700 t500 t300 t2m t200 str sst \
ssr snld snetflx shtfl rh850 rh700 rh500 rh300 rh200 q850 q700 q500 q300 q200 pme msl lhtfl evap ci potevap"

    for v in $vars
    do
        ( files=$(find . -type f -name "era5_${v}_*-*.nc" | sort )

          yyyymmS=$(echo $files | awk '{print $1}'  | sed "s|.*era5_.*\(......\)-\(......\).nc|\1|")
          yyyymmE=$(echo $files | awk '{print $NF}' | sed "s|.*era5_.*\(......\)-\(......\).nc|\2|")

          echo "merging monthly $v from $yyyymmS to $yyyymmE"
          
          if [[ -n $files ]]
          then
              cdo -O -z zip mergetime $files final/era5_${v}.nc
              ofix final/era5_${v}.nc era5_${v} 25
          else
              echo "no monthly files to merge for $v"
          fi
        ) &
        pid0=$!
        pids="$pids $pid0"
        vid[$pid0]=$v
    done

    RESULT=
    for pid in $pids; do
        wait $pid || RESULT="$RESULT $pid"
    done

    if [ -n "$RESULT" ]
    then
        for errcode in $RESULT
        do
            echo "ERROR with monthly merging of ${vid[$errcode]}"
        done
        exit 1
    fi
    pids=
fi

# ----- MONTHLY MEAN OF SOME DAILY VALUES
if (( do_extra ))
then
    cd $finaldst/final
    ( cdo -r -f nc4 -z zip monmean era5_tmin_daily.nc era5_tmin_temp.nc
      cdo -L -O -f nc4 -z zip setreftime,1979-01-15,00:00:00,months \
          -settime,00:00:00 -setday,15 era5_tmin_temp.nc era5_tmin.nc
      ofix era5_tmin.nc era5_tmin 25
      \rm -f era5_tmin_temp.nc
    ) &

    ( cdo -r -f nc4 -z zip monmean era5_tmax_daily.nc era5_tmax_temp.nc
      cdo -L -O -f nc4 -z zip setreftime,1979-01-15,00:00:00,months \
          -settime,00:00:00 -setday,15 era5_tmax_temp.nc era5_tmax.nc
      ofix era5_tmax.nc era5_tmax 25
      \rm -f era5_tmax_temp.nc
    ) &

    wait
fi

# ---- Done processing
t2=$(date +%s)
tr=$(date -d "0 -$t1 sec + $t2 sec" +%T)
echo "# Finished STITCHING at `date '+%F %T'` after ${tr} (hh:mm:ss)"

#_OBSOLETE_ # ----- SEND TO SERVER (OBSOLETE - need new server location)
#_OBSOLETE_ if (( do_sent ))
#_OBSOLETE_ then
#_OBSOLETE_     cd $finaldst/final
#_OBSOLETE_     rsync -vt * oldenbor@bhlclim:/data/storage/climexp/ERA5
#_OBSOLETE_     t3=$(date +%s)
#_OBSOLETE_     tr=$(date -d "0 -$t2 sec + $t3 sec" +%T)
#_OBSOLETE_     echo "# Finished UPLOAD at `date '+%F %T'` after ${tr} (hh:mm:ss)"
#_OBSOLETE_ fi

