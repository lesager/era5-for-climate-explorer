#!/usr/bin/env python3
from ecmwfapi import ECMWFDataServer
 
server = ECMWFDataServer()

server.retrieve({
    "class": "ea",
    "dataset": "era5",
    "date": "19950101",
    "decade": "1990",
    "expver": "1",
    "levelist": "10/20/30/50/70/100/150/200/250/300/400/500/600/700/850/925/1000",
    "levtype": "pl",
    "param": "129.128",
    "grid":'N320',
    "step": "1",
    "stream": "moda",
    "time": "00:00:00",
    "type": "an",
    "target": "/nobackup/users/sager/era5_test.grb",
})
