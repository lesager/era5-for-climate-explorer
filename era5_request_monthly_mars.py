#!/usr/bin/env python2

  #####################################################################
  # Retrieve monthly files of MONTHLY values of ERA5 datasets.        #
  #                                                                   #
  # We retrieve stream=moda, which are "monthly means of daily means" #
  # but accumulated field have been scaled to give units "per day"    #
  #####################################################################

    # Some ERA5 peculiarities: 
    #     sst available at all time step but updated once daily.
    #     all fc here are accumulated, and accumulations are in the hour ending at the forecast step.
    #     In stream=moda, accumulations have been scaled to get units 'per day':
    #        precipitation are given in [m/day] --> factor 1000 to get [mm/day]
    #        radiations/fluxes are provided in [J m**-2/day], should be divided by 86400 seconds
    #        (24 hours) to convert to the commonly used units of Wm-2 

# make the python3-like print behave in python 2
from __future__ import print_function

import calendar
import subprocess
import os, sys, errno
import threading
import cdsapi
from datetime import datetime
import argparse

c = cdsapi.Client()

# --- Accounted for in this module ---
# Note: short names are those used by the ecmwf grib table (Note the kludge: for not-128 table, use 128 table short name. Example: 'atte' instead of 'pev'.)
avail = [
    { "cxname":"t2m",   "short": "t2m",   'long_name': "Near-Surface Air Temperature",  'standard_name': "air_temperature",                     'code': "167.128", 'type': "an", 'units': "K",     'levtype': "sfc"    }, 
    { "cxname":"msl",   "short": "msl",   'long_name': "Sea Level Pressure",            'standard_name': "air_pressure_at_sea_level",           'code': "151.128", 'type': "an", 'units': "Pa",    'levtype': "sfc"    }, 
    { "cxname":"u10",   "short": "u10m",  'long_name': "Eastward Near Surface Wind",    'standard_name': "eastward_wind",                       'code': "165.128", 'type': "an", 'units': "m/s",   'levtype': "sfc"    }, 
    { "cxname":"v10",   "short": "v10m",  'long_name': "Northward Near Surface Wind",   'standard_name': "northward_wind",                      'code': "166.128", 'type': "an", 'units': "m/s",   'levtype': "sfc"    }, 
    { "cxname":"ci",    "short": "ci",    'long_name': "Sea Ice Cover",                 'standard_name': "sea_ice_cover",                       'code':  "31.128", 'type': "an", 'units': "1",     'levtype': "sfc"    }, 
    { "cxname":"snld",  "short": "sd",    'long_name': "snow amount in metres of water equivalent", 'standard_name': "lwe_thickness_of_surface_snow_amount", 'code': "141.128", 'type': "an", 'units': "m",     'levtype': "sfc"    }, 
    { "cxname":"sst",   "short": "sstk",  'long_name': "Surface Temperature",           'standard_name': "surface_temperature",                 'code':  "34.128", 'type': "an", 'units': "K",     'levtype': "sfc"    }, 
    { "cxname":"wspd",  "short": "ws10",  'long_name': "Near Surface Wind Speed",       'standard_name': "wind_speed",                          'code': "207.128", 'type': "an", 'units': "m/s",   'levtype': "sfc"    },
    { "cxname":"tp",    "short": "tp",    'long_name': "Precipitation",                 'standard_name': "precipitation_flux",                  'code': "228.128", 'type': "fc", 'units': "mm/dy", 'factor': 1000.     }, 
    { "cxname":"ls",    "short": "ls",    'long_name': "Large-Scale Precipitation",     'standard_name': "large_scale_precipitation_flux",      'code': "142.128", 'type': "fc", 'units': "mm/dy", 'factor': 1000.     }, 
    { "cxname":"cp",    "short": "cp",    'long_name': "Convective Precipitation",      'standard_name': "convective_precipitation_flux",       'code': "143.128", 'type': "fc", 'units': "mm/dy", 'factor': 1000.     }, 
    { "cxname":"evap",  "short": "e",     'long_name': "Evaporation",                   'standard_name': "water_evaporation_flux",              'code': "182.128", 'type': "fc", 'units': "mm/dy", 'factor': -1000.    }, 
    { "cxname":"ssr",   "short": "ssr",   'long_name': "Net Surface Solar Radiation",   'standard_name': "surface_net_shortwave_flux_in_air",   'code': "176.128", 'type': "fc", 'units': "W/m2",  'factor': 1./86400. }, 
    { "cxname":"str",   "short": "str",   'long_name': "Net Surface Thermal Radiation", 'standard_name': "surface_net_longwave_flux_in_air",    'code': "177.128", 'type': "fc", 'units': "W/m2",  'factor': 1./86400. }, 
    { "cxname":"lhtfl", "short": "slhf",  'long_name': "Surface Latent Heat Flux",      'standard_name': "surface_upward_latent_heat_flux",     'code': "147.128", 'type': "fc", 'units': "W/m2",  'factor': 1./86400. }, 
    { "cxname":"shtfl", "short": "sshf",  'long_name': "Surface Sensible Heat Flux",    'standard_name': "surface_upward_sensible_heat_flux",   'code': "146.128", 'type': "fc", 'units': "W/m2",  'factor': 1./86400. }, 
    { "cxname":"z",     "short": "z",     'long_name': "Geopotential Height",           'standard_name': "geopotential_height",                 'code': "129.128", 'type': "an", 'units': "m2/s2", 'levtype': "pl"     }, 
    { "cxname":"t",     "short": "t",     'long_name': "Air Temperature",               'standard_name': "air_temperature",                     'code': "130.128", 'type': "an", 'units': "K",     'levtype': "pl"     }, 
    { "cxname":"u",     "short": "u",     'long_name': "Eastward Wind",                 'standard_name': "eastward_wind",                       'code': "131",     'type': "an", 'units': "m/s",   'levtype': "pl"     }, 
    { "cxname":"v",     "short": "v",     'long_name': "Northeard Wind",                'standard_name': "northward_wind",                      'code': "132",     'type': "an", 'units': "m/s",   'levtype': "pl"     }, 
    { "cxname":"w",     "short": "w",     'long_name': "omega",                         'standard_name': "lagrangian_tendency_of_air_pressure", 'code': "135.128", 'type': "an", 'units': "Pa/s",  'levtype': "pl"     }, 
    { "cxname":"q",     "short": "q",     'long_name': "Specific Humidity",             'standard_name': "specific_humidity",                   'code': "133.128", 'type': "an", 'units': "kg/kg", 'levtype': "pl"     }, 
    { "cxname":"rh",    "short": "r",     'long_name': "Relative Humidity",             'standard_name': "relative_humidity",                   'code': "157.128", 'type': "an", 'units': "%",     'levtype': "pl"     }, 
    { "cxname":"vap",   "short": "tcwv",  'long_name': "Total column water vapour",     'standard_name': "atmosphere_water_vapor_content",      'code': "137.128", 'type': "an", 'units': "kg/m2", 'levtype': "sfc"    },
    { "cxname":"potevap", "short": "atte",'long_name': "Potential Evaporation",         'standard_name': "potential_evaporation",               'code': "251.228", 'type': "fc", 'units': "mm/dy", 'levtype': "sfc", 'factor': -1000. } 
    ]


class ExceptionThread(threading.Thread):
    """
    Overload to catch exception in parent thread when join method is called.
    """
    def run(self):
        self.exc = None
        try:
            if hasattr(self, '_Thread__target'):
                # Thread uses name mangling prior to Python 3.
                self.ret = self._Thread__target(*self._Thread__args, **self._Thread__kwargs)
            else:
                self.ret = self._target(*self._args, **self._kwargs)
        except BaseException as e:
            self.exc = e

    def join(self):
        super(ExceptionThread, self).join()
        if self.exc:
            raise self.exc
        return self.ret

def makepdir(path):
    """
    makedirs that avoid race conditions.
    Note does not handle cases like 'the dir exists but is not a dir' for example.
    """
    try: 
        os.makedirs(path)
    except OSError as error:
        if error.errno != errno.EEXIST: raise

            
def printThread(msg):
    print('{}: {}'.format(threading.currentThread().name, msg) )
    
    
def retrieve_monthly_era5(topdir, yearStart, varlist=[],
                          yearEnd=None, monthStart=1, monthEnd=12, dwnld=True, cdo=True,
                          clean=False):
    """
    Retrieve monthly ERA5 data from stream=moda.

    By default a full year is downloaded [dwnld] and post_processed
    with cdo/ncatted (cdo). More years can be downloaded if [yearEnd]
    is set. Download can be limited to one or few months with the
    'month' keywords.

    varlist: List of names. Either their ECMWF grib table shortnames or Climate Explorer names

    For efficient retrieval, data requests are grouped per month.
    Note: Only 3 retrievals at a time allowed by ECMWF.
    """        

    makepdir(topdir)
    if not yearEnd: yearEnd = yearStart

    # Degenerated cases
    if not cdo and not dwnld and not clean:
        print("\n\t Neither download nor postprocessing nor cleaning of data requested!!")
        return 2
    
    if not varlist:
        print("\n\t No variables requested!!")
        return 2
        
    # -- Check availability and get groups of requests
    an_sfc, an_plev, fc_sfc = build_request_dicts(varlist)

    # -- Loop over months
    starttime = datetime.utcnow()
    
    for year in range(yearStart, yearEnd + 1):
        for month in range(monthStart, monthEnd + 1):

            startDate = '%04d%02d%02d' % (year, month, 1)
            target = os.path.join(topdir,"era5_monthly_%04d%02d_" % (year, month))

            if clean:
                if an_sfc:
                    tgt = target+'_moda_an_sfc.grb'
                    if os.path.isfile(tgt): os.remove(tgt)
                if an_plev:
                    tgt = target+'_moda_an_pl.grb'
                    if os.path.isfile(tgt): os.remove(tgt)
                if fc_sfc:
                    tgt = target+'_moda_fc_sfc.grb'
                    if os.path.isfile(tgt): os.remove(tgt)
            else:
                # retrieve files and/or postprocess in parallel
                if an_sfc:
                    tgt = target+'_moda_an_sfc.grb'
                    t1 = ExceptionThread(target=era5_request_sfc_moda, args=('an', '/'.join(an_sfc), startDate, tgt, dwnld, cdo))
                    t1.start()
                if an_plev:
                    tgt = target+'_moda_an_pl.grb'
                    t2 = ExceptionThread(target=era5_request_an_pl_moda, args=('/'.join(an_plev), startDate, tgt, dwnld, cdo))
                    t2.start()
                if fc_sfc:
                    tgt = target+'_moda_fc_sfc.grb'
                    t3 = ExceptionThread(target=era5_request_sfc_moda, args=('fc', '/'.join(fc_sfc), startDate, tgt, dwnld, cdo))
                    t3.start()

                for k in threading.enumerate():
                    if k.name != 'MainThread':
                        try:
                            k.join()
                        except Exception as e:
                            print('\n Error raised in {} for month {}-{:02}:'.format(k.name, year, month) )
                            print(e.message )
                            # Re-raise: this will print traceback and return 1 if
                            # called from __main__, other threads will finish, but
                            # the for-loop will stop (this is the point of
                            # ExceptionThread: without it you'd keep looping
                            # despite exceptions):
                            raise
                
    # -- Epilog
    stime = datetime.utcnow() - starttime
    print('\n*II* Processing time {} sec.'.format(stime.total_seconds()) )

    
def build_request_dicts(iwant):
    """
    Returns a list of requested grib codes for each of the three
    STREAM/TYPE/LEVELTYPE considered so far.

    IWANT: (list of string) short names (as in ECMWF GRIB table) or
           Climate Explorer names of the wanted variables.    
    """
    
    # Not available
    shorts = [d['short'] for d in avail]
    cxname = [d['cxname'] for d in avail]
    missing = [d for d in iwant if d not in shorts and d not in cxname]
    if missing: raise Exception('\n\tvariable(s) not available: '+' '.join(missing))
    
    # We can group data request for same stream/type/leveltype 
    an_sfc = [d for d in avail if d['type'] == 'an' and d.get('levtype','') == 'sfc']
    an_ple = [d for d in avail if d['type'] == 'an' and d.get('levtype','') == 'pl']
    fc_sfc = [d for d in avail if d['type'] == 'fc']

    # Limit to the wanted one
    was = [d['code'] for d in an_sfc if d['short'] in iwant or d['cxname'] in iwant]
    wap = [d['code'] for d in an_ple if d['short'] in iwant or d['cxname'] in iwant]
    wfs = [d['code'] for d in fc_sfc if d['short'] in iwant or d['cxname'] in iwant]

    return was, wap, wfs
    

def cdo_postproc(params, yyyymmdd, infile):
    """
    All params (and only them) are in the INFILE by design.
    """
    
    # destination is a subdir
    topdir = os.path.dirname(infile)
    dest = os.path.join(topdir, 'monthly', yyyymmdd[0:4])
    makepdir(dest)

    # -- splitname
    cmd = 'cdo -t ecmwf splitname {} {}/era5_{}_'.format(infile, dest, yyyymmdd[0:6])
    printThread( 'split files with "{}"'.format(cmd))
    p = subprocess.Popen( cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout_str,stderr_str = p.communicate()

    if p.returncode:
        print(stderr_str)
        raise RuntimeError('\n\terror while cdo-splitting {}'.format(infile))
    
    # -- cdo+nco
    cdo = "cdo -r -b 32 -f nc4 -z zip setgridtype,regular "

    for p in params.split('/'):
        pdes = [d for d in avail if d['code']==p][0]
        printThread(
            "postprocessing parameter {}: {}/{} ({})".format(
                p,
                pdes['short'].upper(),
                pdes['cxname'],
                pdes.get('levtype','')))

        gribf = '{}/era5_{}_{}.grb'.format(dest, yyyymmdd[0:6], pdes['short'].upper() )  # grib file name expected from splitname above

        netcf = gribf.replace( pdes['short'].upper()+'.grb', pdes['cxname']+'.nc'      ) # netCDF file name after cdo
        atted = gribf.replace( pdes['short'].upper()+'.grb', pdes['cxname']+'_atted.nc') # netCDF file name after ncatted
        
        # - convert, scale and/or rename
        if os.path.exists(gribf):

            cmd = ( cdo + " -setname," + pdes['cxname'] + " -settaxis," + yyyymmdd[0:4] +
                    "-"+yyyymmdd[4:6]+"-15,0:00,1mon " + gribf + ' ' + netcf )

            if pdes.get('factor',1.) != 1.:
                cmd = cmd.replace( 'setname', "mulc," + str(pdes['factor']) + " -setname" )

            printThread( 'process file {} with "{}"'.format(gribf,cmd))

            p = subprocess.Popen( cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout_str, stderr_str = p.communicate()

            if p.returncode:
                print(stdout_str, stderr_str)
                raise RuntimeError('\n\terror with command: {}'.format(cmd))
        else:
            raise NameError(gribf)
        
        # - attributes
        cmd = ['ncatted', '-O',
                '-a', 'long_name,' + pdes['cxname'] + ",m,c," + pdes['long_name'], 
                '-a', 'standard_name,' + pdes['cxname'] + ",m,c," + pdes['standard_name'], 
                '-a', 'units,' + pdes['cxname'] + ",m,c," + pdes['units'], 
                '-a', 'title,global,m,c,"ERA5 reanalysis, https://www.ecmwf.int/en/forecasts/datasets/reanalysis-datasets/era5"',
                netcf, atted]        
        p = subprocess.Popen( cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout_str, stderr_str = p.communicate()
        
        if p.returncode:
            print(stdout_str, stderr_str)
            raise RuntimeError('\n\terror with command: {}'.format(' '.join(cmd)))

        # - clean
        os.rename(atted, netcf)
        os.remove(gribf)

        # - extra: extract some levels, zonal averages for T and U 
        if pdes.get('levtype','') == 'pl':

            levellist = [ 850, 700, 500, 300, 200 ]
            for level in levellist:
                levelfile = netcf.replace('.nc', str(level)+'.nc')
                cmd = ['cdo', '-z', 'zip', 'sellevel,' + str(level) + '00.', netcf, levelfile]
                rc = subprocess.call(cmd)
                if rc: raise RuntimeError('\n\terror with command: {}'.format(' '.join(cmd)))

            if pdes['cxname'] == "t" or pdes['cxname'] == "u":
                zonfile = netcf.replace('.nc', 'zon.nc')
                cmd = ['cdo', '-z', 'zip', 'zonmean', netcf, zonfile]
                rc = subprocess.call(cmd)
                if rc: raise RuntimeError('\n\terror with command: {}'.format(' '.join(cmd)))


def era5_request_sfc_moda(dtype, params, requestMonth, target, dwnld, cdo):
    """
    Retrieve TYPE/SFC/MODA: analysis|forecast / surface / HRES monthly means of daily means
    dtype        : (string) 
    requestMonth : (string) YYYYMM01
    target       : (string) output filename
    params       : (string) something like "31.128/34.128/137.128/141.128"
    """
    if not os.path.exists(target) and dwnld:
        decade = requestMonth[0:3]+'0'
        printThread("About to retrieve {}".format(target))

        try:
            c.retrieve("reanalysis-era5-complete", {
                "class": "ea",
                "dataset": "era5",
                "date": requestMonth,
                "decade": decade,
                "expver": "1",
                "levtype": "sfc",
                "param": params,
                "stream": "moda",
                "type": dtype,
            }, target)
        except:
            print("*EE* error during retrieval, removing target {}".format(target) )
            if os.path.exists(target): os.remove(target)
            raise

    if cdo:
        if os.path.exists(target): cdo_postproc(params, requestMonth, target)
        else: raise NameError(target)


def era5_request_an_pl_moda(params, requestMonth, target, dwnld, cdo):
    """"
    Retrieve AN/PL/MODA: analysis/pressure_levels/HRES monthly means of daily means
      requestMonth : (string) YYYYMM01
      target       : (string) output filename
      params       : (string) something like "129.128/130.128/131.128/132.128/133.128/135.128/157.128"
    Retrieve data on the reduced Gaussian grid that correspond to the spectral truncation.
    """
    if not os.path.exists(target) and dwnld:
        decade = requestMonth[0:3]+'0'
        printThread("About to retrieve {}".format(target))
        try:
            c.retrieve("reanalysis-era5-complete", {
                "class": "ea",
                "dataset": "era5",
                "date": requestMonth,
                "decade": decade,
                "expver": "1",
                "levelist": "10/20/30/50/70/100/150/200/250/300/400/500/600/700/850/925/1000",
                "levtype": "pl",
                "grid":'N320',
                "param": params,
                "stream": "moda",
                "type": "an",
                }, target)
        except:
            print("*EE* error during retrieval, removing target {}".format(target) )
            if os.path.exists(target): os.remove(target)
            raise

    if cdo:
        if os.path.exists(target): cdo_postproc(params, requestMonth, target)
        else: raise NameError(target)
            
            
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Retrieve and process ERA5 for climate explorer')

    parser.add_argument("outdir",                help="dir where data are downloaded. Created if needed. Once processed through cdo/nco, the data are found in DIR/monthly/YYYY", metavar='DIR')
    parser.add_argument("first_year", type=int,  help="First year to retrieve and/or process")
    parser.add_argument("--vlist",    nargs='*', help="List of variables to process", metavar='VAR')
    parser.add_argument("--last",     type=int,  help="Last year to retrieve and/or process. Default to first_year.")
    parser.add_argument("--mm1",      type=int,  help="first month to process. Default to 1.", metavar='MonthStart', default=1)
    parser.add_argument("--mm2",      type=int,  help="last month to process. Default to 12.", metavar='MonthEnd',   default=12)
    parser.add_argument("--no-dwnld",            help="do not retieve data",                   action="store_true",  default=False)
    parser.add_argument("--no-cdo",              help="do not process retrieval with cdo/nco", action="store_true",  default=False)
    parser.add_argument("--clean",               help="remove downloaded grib files",          action="store_true",  default=False)

    args = parser.parse_args()

    rc = retrieve_monthly_era5(
        args.outdir, args.first_year, varlist=args.vlist,
        yearEnd=args.last, monthStart=args.mm1, monthEnd=args.mm2,
        dwnld=not args.no_dwnld, cdo=not args.no_cdo, clean=args.clean)
    
    if rc == 2:
        print('')
        parser.print_help()
        
    sys.exit(rc)

