#!/usr/bin/env python

import sys
import os, glob
import shutil
from netCDF4 import Dataset    

if __name__=='__main__':

    variable_name = 'potevap'

    fnames='/nobackup_1/users/sager/CX/TMP/monthly/2020/era5_2020??_potevap.nc'
    fnames='/nobackup_2/users/sager/ERA5/????/mon/era5_potevap*.nc'
    files=glob.glob(fnames)

    for file_name in files:

        print(file_name)
        
        #backup
        file_name_bak = '{}.bak'.format(file_name)
        if os.path.isfile(file_name_bak):
            raise RuntimeError('Backup file "{}" exists already!'.format(file_name_bak))

        shutil.copy2(src=file_name, dst=file_name_bak)

        dataset = Dataset(file_name, 'r+')
        try:
            data = -1.*dataset.variables[variable_name][:]
        except KeyError:
            raise RuntimeError('Variable "{}" not found in "{}"!'.format(variable_name, file_name))

        dataset.variables[variable_name][:] = data
        dataset.close()
