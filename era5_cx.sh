#! /usr/bin/env bash

set -e

 #########################################################################
 # This is a simple wrapper around the main python scripts that          #
 # retrieves ERA5 data and cast them into a Climate Explorer friendly    #
 # format.                                                               #
 #                                                                       #
 # For details and more options, try:                                    #
 #     ./era5_request_monthly_cds.py -h                                  #
 #     ./era5_request_daily_cds.py -h                                    #
 #                                                                       #
 # The variables to process and the output directory are hardcoded here. #
 # See PARAMETERS section.                                               #
 #                                                                       #
 # Two modes of retrievals available:                                    #
 #  - update: retrieve the latest month available [BROKEN]               #
 #  - archive: for any specific period, specify at the command line      #
 #########################################################################

usage() {
    cat <<EOT >&2

usage: ${0##*/} [-u] [-p] [-m] [-d] [-s MonStart] [-e MonEnd] YSTART [YEND]

    YSTART      : First year to process
    YEND        : Last year (optional, default to first year)
    -b          : bulk processing. Use with -m when processing entire year at once to speed up.
    -m          : monthly averages
    -d          : daily averages (or max or min or sum)
    -s MonStart : first month to process. Default to 1.
    -e MonEnd   : last month to process. Default to 12.
    -p          : only update the Previous month daily forecast variables

    -i          : Ignore some errors (a developer tool: currently let you run only the postprocessing done here in the daily case)
    -u          : Check the latest data retrieved, and update daily OR monthly data with following month.
                  YSTART is ignored and not needed.

 TYPICAL USAGES:

     ** Get monthly averages for the entire year YYYY
       ${0##*/} -b -m YYYY |& tee log/YYYY.log

     ** Get monthly averages for year YYYY and month MM (Replace -m with -d for daily data)
       ${0##*/} -m -s MM -e MM YYYY |& tee log/YYYY.log

     ** Get monthly averages for year YYYY and months FIRST to LAST (Replace -m with -d for daily data)
       ${0##*/} -m -s FIRST -e LAST YYYY |& tee log/YYYY.log

     ** Add one month to the latest month already processed of DAILY data
       ${0##*/} -ud

     ** Add one month to the latest month already processed of MONTHLY data
       ${0##*/} -um
EOT
}

# --------- HARDCODED PARAMETERS
odir='/nobackup_1/users/sager/CX/TMP' # TMP output dir where data are retrieved and intermediate files written
finaldst='/net/pc230042/nobackup/users/sager/nobackup_2_old/ERA5' # dir with FINAL product

#  monthly variables ('atte' is truly 'pev')
mwanted="t2m msl u10 v10 ws10 ci snld sst tcwv tp e slhf sshf ssr str z t u v w q r atte"

#  daily variables - separated between analysis and forecast data (modified if -p set)
#
#  Note that dwanted_an variables are moved to finaldst with globbing
#  (ie sthg like: mv *$var ...). That means you should be carefully
#  with any variable name that contains the name of another dwanted_an
#  variable
dwanted_an="t2m tdew msl sp z500 t500 q500 u10 v10"
dwanted_fc="tmin tmax tp evap"

# # subset (set and uncomment)
# dwanted_an=
# dwanted_fc="tmax evap"


# To avoid issue with "non-thread-safe NetCDF4/HDF5 library in a multi-threaded environment"
cdo='cdo -L'

# -------- OPTIONS
latest=0
ignore=0
bulk=0
daily_forecast_only=0

while getopts :pubdmis:e: OPT; do
    case $OPT in
        u) latest=1 ;;
        p) daily_forecast_only=1 ;;
        b) bulk=1 ;;
        d) daily=1 ;;
        m) monthly=1 ;;
        s) M1=$OPTARG ;;
        e) M2=$OPTARG ;;
        i) ignore=1 ;;
        *) usage
           exit 2
    esac
done
shift $(( OPTIND - 1 ))
OPTIND=1

# Function `lastday' print the number of day in a month.
#
#    Usage: lastday YYYY MM
#
lastday()  {
    local mlength=(xx 31 28 31 30 31 30 31 31 30 31 30 31)
    local year=$1
    local month=$2
    local leap

    if [[ month -ne 2 ]]
    then
        echo ${mlength[month]}
    else
        leap=0
        (( ! $(($year % 4)) )) && \
            ( (( $(($year % 100)) )) || (( ! $(($year % 400)) )) ) && leap=1
        echo $(( 28 + leap ))
    fi
}

# --------- Overwrite all settings if UPDATING with one month
if (( latest ))
then
        pushd $finaldst
        if (( daily ))
        then
            lastf=$(ls */day/era5_*_t2m.nc | tail -1)
            lastm=$(cdo -s showmon $lastf)
            lasty=$(cdo -s showyear $lastf)
            monthly=0
            SERIES=DAILY
        elif (( monthly ))
        then
            lastf=$(ls */mon/era5_t2m_*.nc | tail -1)
            lastm=$(cdo -s showmon $lastf | awk '{print $NF}')
            lasty=$(cdo -s showyear $lastf)
            #daily=0
            SERIES=MONTHLY
        else
            echo "*EE* You must specify daily or monthly update"
            usage
            exit 2
        fi
        popd
        
        Y1=$lasty
        M1=$lastm
        if [ $M1 -eq 12 ]
        then
            Y1=$(( Y1+1 ))
            M1=1
        else
            M1=$(( ${M1}+1 ))
        fi
        
        # Check availability of a monthly update. Data are provided
        # with about 5-day lag. Use 5 days lag to be sure.
        d1=$(lastday $Y1 $M1)
        if [[ $(date -d "6 days ago" +%s) -gt $(date -d $Y1-$M1-$d1 +%s) ]]
        then
            printf "*II* Will update $SERIES series with ${Y1}-${M1}\n"
            M2=$M1
            set $Y1
        else
            printf "*II* No new data to update $SERIES series"
            exit 0
        fi
fi

# --------- YEARS ARGS
case $# in
    1) Y1=$1
       Y2=$1
       ;;
    2) Y1=$1
       Y2=$2        
       ;;
    *) printf "\n\tRequires one or two arguments!!\n\n"
       usage
       exit 1            
esac

set $(printf "$Y1\n$Y2" | sort -g) # ensure increasing order
Y1=$1
Y2=$2

# --------- MONTHS
[[ -z $M1 ]] && M1=1
[[ -z $M2 ]] && M2=12

set $(printf "$M1\n$M2" | sort -g)
M1=$1
M2=$2

# args for era5_request_*.py
[[ -n $Y2 ]] && options=" --last $Y2"
[[ -n $M1 ]] && options=$options" --mm1 $M1"
[[ -n $M2 ]] && options=$options" --mm2 $M2"

cwd=$PWD


(( daily_forecast_only )) && dwanted_an=

# --------- WORK - MONTHLY DATA
if (( monthly ))
then
    if (( bulk ))
    then
        # download only first
        echo "Downloading: monthly $odir $Y1 $options --vlist $mwanted"
        ./era5_request_monthly_cds.py $odir $Y1 $options --vlist $mwanted --no-cdo
    fi
    # Download if not done + cdo post-processing
    echo "Calling: ./era5_request_monthly.py $odir $Y1 $options --vlist $mwanted"
    ./era5_request_monthly_cds.py $odir $Y1 $options --vlist $mwanted

    # This is done only on month 12 when -b is set
    if (( ! bulk )) || [[ $M2 == 12 ]]; then
    for year in $(eval echo {$Y1..$Y2})
    do
        cd $odir/monthly/${year}

        dst=${finaldst}/${year}/mon
        mkdir -p $dst

        ST1=$(printf '%02d' ${M1})
        stamp=_${year}01-${year}12

        # - Concatenate monthly files into one yearly file

        for var in $(ls era5_${year}${ST1}_* | sed -r "s/era5_${year}${ST1}_(.*)\.nc/\1/g")
        do
            # overwrite any pre-existing (i.e. not full year is accounted for) file in final dir (-O)
            # Assumption is that ALL required files are present (not ok to partially update an already completed year)
            $cdo -O -z zip copy era5_${year}*_${var}.nc ${dst}/era5_${var}${stamp}.nc

            # remove only complete year (more rigorous should check on all 12 files)
            if [[ ( -f era5_${year}01_${var}.nc ) && ( -f era5_${year}12_${var}.nc ) ]]
            then
                for k in {1..12}
                do
                    \rm -f era5_${year}$(printf '%02d' ${k})_${var}.nc
                done
            fi
        done

        # - Extra processing if some variables have been retrieved
        printf "\n\tCompute derived variables...\n"
        cd $dst
        
        rtp="(^| )tp( |$)"
        revap="((^| )e( |$))|((^| )evap( |$))"
        if [[ $mwanted =~ $rtp && $mwanted =~ $revap ]]
        then
            $cdo -O -z zip sub era5_tp${stamp}.nc era5_evap${stamp}.nc era5_pme${stamp}.nc
        fi
        
        relh="((^| )slhf( |$))|((^| )lhtfl( |$))"
        resh="((^| )sshf( |$))|((^| )shtfl( |$))"        
        if [[ $mwanted =~ $relh && $mwanted =~ $resh ]]
        then
            $cdo -O -z zip add era5_shtfl${stamp}.nc era5_lhtfl${stamp}.nc era5_trbflx${stamp}.nc
            ncrename -v shtfl,trbflx era5_trbflx${stamp}.nc
        fi
        
        rssr="(^| )ssr( |$)"
        rstr="(^| )str( |$)"
        if [[ $mwanted =~ $rssr && $mwanted =~ $rstr ]]
        then
            $cdo -O -z zip add era5_ssr${stamp}.nc era5_str${stamp}.nc noot.nc
            $cdo -O -z zip add era5_trbflx${stamp}.nc noot.nc era5_snetflx${stamp}.nc
            ncrename -v trbflx,snetflx era5_snetflx${stamp}.nc
            \rm -f noot.nc
        fi
    done
    fi

    cd $cwd

    # clean up (remove the downloaded grib files) if no error happened (caught thru set -e)
    # Doing this here allows for faster re-processing (download is skipped) if something went wrong
    printf "\n\tCalling: ./era5_request_monthly.py --CLEAN !\n"
    ./era5_request_monthly_cds.py $odir $Y1 $options --vlist $mwanted --clean

    printf "\n\t **SUCCESS**\n"
fi

# --------- WORK - DAILY DATA
if (( daily ))
then
    if (( ! ignore ))
    then
        echo "Calling: ./era5_request_daily_cds.py $odir $Y1 $options --vlist $dwanted_an $dwanted_fc"
        ./era5_request_daily_cds.py $odir $Y1 $options --vlist $dwanted_an $dwanted_fc
    fi

    for year in $(eval echo {$Y1..$Y2})
    do
        cd $odir/daily/${year}
        
        dst=${finaldst}/${year}/day
        dstbef=$dst
        mkdir -p $dst
        
        for month in $(eval echo {$M1..$M2})
        do
            # monthly stamps and files basename (w/o variable name)
            mm=$(printf '%02d' ${month})
            stamp_now=$year$mm
            
            if [[ $month -eq 1 ]]; then
                mmb=12
                stamp_before=$((year-1))12
                fbase_before=../$((year-1))/era5_${stamp_before}
                dstbef=${finaldst}/$((year-1))/day/era5_${stamp_before}
            else
                mmb=$(printf '%02d' $((month-1)) )
                stamp_before=$year${mmb}
                fbase_before=era5_${stamp_before}
                dstbef=${dst}/era5_${stamp_before}
            fi

            if [[ $month = 12 ]]; then
                stamp_after=$((year+1))01
                stamp_edge_case=${year}01
                fbase_after=../$((year+1))/era5_${stamp_after}
            else
                stamp_after=$year$(printf '%02d' $((month+1)) )
                fbase_after=era5_${stamp_after}
                stamp_edge_case=${year}12
            fi

            # Special treatment of forecast variables, because:
            # MARS requests: the first 6hr of the month are missing in
            #    forecast fields (but available in the previous month)
            # CDS requests: the last hour of the month is missing (but
            #    available in the next month)
            for varin in $dwanted_fc
            do
                var=$varin
                
                # daily operator and name mangling
                case ${varin} in
                    e ) dayop=daysum
                        var=evap
                        ;;
                    tp|evap) dayop=daysum
                             ;;
                    tmin) dayop=daymin
                          ;;
                    mn2t) dayop=daymin
                          var=tmin
                          ;;
                    tmax) dayop=daymax
                          ;;
                    mx2t) dayop=daymax
                          var=tmax
                          ;;
                    *) echo "FC variable '${varin}' is unaccounted for. Please fix."
                       exit 1
                esac

                printf "\n*II* Processing FC variable: ${var}, for $stamp_now\n"
                
                current=era5_${stamp_now}_${var}.nc
                before=${fbase_before}_${var}.nc
                after=${fbase_after}_${var}.nc

                # sanity check
                if [[ ! -f $current ]]
                then
                    printf "\t before  : ${before}\n"
                    printf "\t current : ${current}\n"
                    printf "\t after   : ${after}\n"
                    if (( ignore ))
                    then
                        echo "Skip missing file '$current', assuming it has already been processed"
                        continue
                    else
                        echo "Something went wrong! File '$current' does not exists!"
                        exit 1
                    fi
                fi

                # -- CDS retrievals
                if [[ -f $before ]]
                then
                    # This works becuse dayop satisfies:
                    #    OP( OP(1,...,N-1), N ) = OP(1,...,N)
                    printf "\n\t UPDATE last day of $before with $current!\n"
                    $cdo mergetime $before $current aap_$$_$var.nc
                    $cdo splitmon -${dayop} aap_$$_$var.nc ${var}_$$_ 
                    $cdo -O -f nc4 -z zip settime,12:00:00 ${var}_$$_${mmb}.nc ${dstbef}_${var}.nc
                    \rm -f aap_$$_$var.nc
                    \rm -f ${var}_$$_*.nc

                    # and remove the before file since it has been
                    # updated (we assume that older files do not need
                    # update, ELSE COMMENT THIS LINE)
                    \rm -f $before
                fi

                if (( ! daily_forecast_only ))
                then
                    # dayop is not needed here (data are already reduced)
                    printf "\n\t PROCESS $current (remove previous-month-data from it)\n"
                    $cdo splitmon -${dayop} $current ${var}_$$_ 
                    $cdo -O -f nc4 -z zip settime,12:00:00 ${var}_$$_${mm}.nc $dst/${current}
                    \rm -f ${var}_$$_*.nc
                fi
            done                # Done Forecast Variables

            # -- Move other variables to final dir

            for var in $dwanted_an
            do
                case $var in
                    u10|v10|u10m|v10m)
                        continue ;;
                    *)
                        printf "\n*II* Processing AN variable: ${var}, for $stamp_now\n"
                        if (( ignore )) && [[ ! -f era5_${stamp_now}_${var}.nc ]]
                        then
                            printf "\tskipping missing era5_${stamp_now}_${var}.nc\n"
                        else
                            mv -f era5_${stamp_now}_*${var}.nc $dst
                        fi
                esac
            done
            
            # special treatment for u10m/v10m
            do_wind=0
            ru10="(^| )u10m?( |$)"
            rv10="(^| )v10m?( |$)"
            [[ $dwanted_an =~ $ru10 && $dwanted_an =~ $rv10 ]] && do_wind=1

            if (( do_wind ))
            then
                [[ -f era5_${stamp_now}_u10.nc && -f era5_${stamp_now}_v10.nc ]] && missing_file=0 || missing_file=1
                if (( ignore )) && (( missing_file ))
                then
                    printf "\n*II* Skipping wind variables, for $stamp_now\n\n"                    
                else
                    printf "\n*II* Processing wind variables, for $stamp_now\n\n"

                    # compute wind speed
                    $cdo merge era5_${stamp_now}_u10.nc  era5_${stamp_now}_v10.nc  winds.nc
                    $cdo expr,'wspd=sqrt(u10^2+v10^2)' winds.nc wspd.nc
                    \rm -f winds.nc

                    # get its daily mean
                    $cdo -z zip chname,wspd,sfcWind -settime,12:00:00 -daymean wspd.nc mean_wind.nc
                    ncatted -h -O \
                            -a long_name,sfcWind,o,c,"Daily-Mean Near-Surface Wind Speed" \
                            -a standard_name,sfcWind,o,c,"wind_speed" \
                            mean_wind.nc ${dst}/era5_${stamp_now}_sfcWind.nc

                    # and its daily max
                    $cdo -z zip chname,wspd,sfcWindmax -settime,12:00:00 -daymax wspd.nc max_wind.nc
                    ncatted -h -O \
                            -a long_name,sfcWindmax,o,c,"Daily-Max Near-Surface Wind Speed" \
                            -a standard_name,sfcWindmax,o,c,"wind_speed" \
                            max_wind.nc ${dst}/era5_${stamp_now}_sfcWindmax.nc

                    \rm -f wspd.nc mean_wind.nc max_wind.nc era5_${stamp_now}_u10.nc  era5_${stamp_now}_v10.nc
                fi
            fi
            
        done
    done

    # Clean up
    cd $cwd
    printf "\n\tCalling: ./era5_request_daily.py --CLEAN !\n"
    ./era5_request_daily_cds.py $odir $Y1 $options --vlist $dwanted_an $dwanted_fc --clean
    
    printf "\n\t **SUCCESS**\n"
fi
