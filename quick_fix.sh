#! /usr/bin/env bash

set -e

finaldst='/nobackup_2/users/sager/ERA5' # dir with final product


cd $finaldst

# -- fix a variable name and att

# files=$(find . -type f -name 'era5_snd*.nc')

# for ff in ${files}
# do
#     tgt=$(echo $ff | sed "s|snd|snld|" )
#     echo $ff $tgt

#     ncrename -v snd,snld $ff aap.nc
#     ncatted -O \
#             -a long_name,snld,o,c,'snow amount in metres of water equivalent' \
#             -a standard_name,snld,o,c,'lwe_thickness_of_surface_snow_amount' \
#             -a title,global,o,c,'ERA5 reanalysis, https://www.ecmwf.int/en/forecasts/datasets/reanalysis-datasets/era5' \
#             aap.nc $tgt
#     \rm aap.nc
#     echo
# done



# -- stich and send to CX server

var=snld
files=$(find . -type f -name "era5_${var}*.nc" | sort )

cdo -z zip  mergetime $files era5_${var}.nc

rsync -vt era5_${var}.nc oldenbor@bhlclim:/data/storage/climexp/ERA5


