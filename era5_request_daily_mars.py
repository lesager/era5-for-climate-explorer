#!/usr/bin/env python

 ###########################################################
 # Retrieve monthly files of DAILY values of ERA5 datasets #
 #                                                         #
 # We retrieve stream=oper, which are HRES sub-daily data  #
 #                                                         #
 ###########################################################

    # Some ERA5 peculiarities: 
    #     sst available at all time step but updated once daily.
    #     all fc here are accumulated, and accumulations are in the hour ending at the forecast step.

# make the python3-like print behave in python 2
from __future__ import print_function

import calendar
import subprocess
import os, sys, errno
import shutil
import threading
import cdsapi
from datetime import datetime
import argparse

c = cdsapi.Client()

# HACK folder destination
hackdst='/nobackup_2/users/sager/ERA5/hourly_folmer' 

# --- Accounted for in this module ---
# Note: short names are from the ecmwf grib table, and appear (in uppercase!) in the filename when you do a "cdo splitname"
avail = [
    { "cxname":"t2m",   "short": "t2m",  'long_name': "Near-Surface Air Temperature",               'standard_name': "air_temperature",           'code': "167.128", 'type': "an", 'acc':False, 'day_op':'daymean', 'units': "K",     'levtype': "sfc" }, 
    { "cxname":"tdew",  "short": "d2m",  'long_name': "Near-Surface Dew Temperature",               'standard_name': "dew_temperature",           'code': "168.128", 'type': "an", 'acc':False, 'day_op':'daymean', 'units': "K",     'levtype': "sfc" }, 
    { "cxname":"tmin",  "short": "mn2t", 'long_name': "Daily Minimum Near-Surface Air Temperature", 'standard_name': "air_temperature",           'code': "202.128", 'type': "fc", 'acc':False, 'day_op':'daymin',  'units': "K",     'levtype': "sfc" }, 
    { "cxname":"tmax",  "short": "mx2t", 'long_name': "Daily Maximum Near-Surface Air Temperature", 'standard_name': "air_emperature",            'code': "201.128", 'type': "fc", 'acc':False, 'day_op':'daymax',  'units': "K",     'levtype': "sfc" }, 
    { "cxname":"msl",   "short": "msl",  'long_name': "Sea Level Pressure",                         'standard_name': "air_pressure_at_sea_level", 'code': "151.128", 'type': "an", 'acc':False, 'day_op':'daymean', 'units': "Pa",    'levtype': "sfc" }, 
    { "cxname":"sp",    "short": "sp",   'long_name': "Surface Air Pressure",                       'standard_name': "surface_air_pressure",      'code': "134.128", 'type': "an", 'acc':False, 'day_op':'daymean', 'units': "Pa",    'levtype': "sfc" }, 
    { "cxname":"u10",   "short": "u10m", 'long_name': "Eastward Near Surface Wind",                 'standard_name': "eastward_wind",             'code': "165.128", 'type': "an", 'acc':False, 'day_op':'selhour,0,3,6,9,12,15,18,21', 'units': "m/s",   'levtype': "sfc" }, 
    { "cxname":"v10",   "short": "v10m", 'long_name': "Northward Near Surface Wind",                'standard_name': "northward_wind",            'code': "166.128", 'type': "an", 'acc':False, 'day_op':'selhour,0,3,6,9,12,15,18,21', 'units': "m/s",   'levtype': "sfc" }, 
    { "cxname":"tp",    "short": "tp",   'long_name': "Precipitation",                              'standard_name': "precipitation_flux",        'code': "228.128", 'type': "fc", 'acc':True,  'day_op':'daysum',  'units': "mm/dy", 'levtype': "sfc", 'factor':  1000. }, 
    { "cxname":"evap",  "short": "e",    'long_name': "Evaporation",                                'standard_name': "water_evaporation_flux",    'code': "182.128", 'type': "fc", 'acc':True,  'day_op':'daysum',  'units': "mm/dy", 'levtype': "sfc", 'factor': -1000. }, 
    { "cxname":"z500",  "short": "z",    'long_name': "Geopotential Height",                        'standard_name': "geopotential_height",       'code': "129.128", 'type': "an", 'acc':False, 'day_op':'daymean', 'units': "m2 s-2",'levtype': "pl"  }, 
    { "cxname":"t500",  "short": "t",    'long_name': "Air Temperature",                            'standard_name': "air_temperature",           'code': "130.128", 'type': "an", 'acc':False, 'day_op':'daymean', 'units': "K",     'levtype': "pl"  }, 
    { "cxname":"q500",  "short": "q",    'long_name': "Specific Humidity",                          'standard_name': "specific_humidity",         'code': "133.128", 'type': "an", 'acc':False, 'day_op':'daymean', 'units': "kg/kg", 'levtype': "pl"  }
    ]


class ExceptionThread(threading.Thread):
    """
    Overload to catch exception in parent thread when join method is called.
    """
    def run(self):
        self.exc = None
        try:
            if hasattr(self, '_Thread__target'):
                # Thread uses name mangling prior to Python 3.
                self.ret = self._Thread__target(*self._Thread__args, **self._Thread__kwargs)
            else:
                self.ret = self._target(*self._args, **self._kwargs)
        except BaseException as e:
            self.exc = e

    def join(self):
        super(ExceptionThread, self).join()
        if self.exc:
            raise self.exc
        return self.ret

def makepdir(path):
    """
    makedirs that avoid race conditions.
    Note: does not handle cases like 'the dir exists but is not a dir' for example.
    """
    try: 
        os.makedirs(path)
    except OSError as error:
        if error.errno != errno.EEXIST: raise

            
def printThread(msg):
    print('{}: {}'.format(threading.currentThread().name, msg) )
    
    
def retrieve_daily_era5(topdir, yearStart, varlist=[],
                        yearEnd=None, monthStart=1, monthEnd=12, dwnld=True, cdo=True,
                        clean=False):
    """
    Retrieve monthly ERA5 data from stream=oper.

    By default a full year is downloaded [dwnld] and post_processed
    with cdo/ncatted (cdo). More years can be downloaded if [yearEnd]
    is set. Download can be limited to one or few months with the
    'month' keywords.

    varlist: List of names. Either their ECMWF grib table shortnames or Climate Explorer names

    For efficient retrieval, data requests are grouped per: type_of_data/year/month/type_of_level (tree on MARS).
    Note: Only 3 retrievals at a time allowed by ECMWF.
    """

    makepdir(topdir)
    if not yearEnd: yearEnd = yearStart

    # Degenerated cases
    if not cdo and not dwnld and not clean:
        print("\n\t Neither download nor postprocessing nor cleaning of data requested!!")
        return 2
    
    if not varlist:
        print("\n\t No variables requested!!")
        return 2
        
    # -- Check availability and get groups of requests
    an_sfc, an_plev, fc_sfc = build_request_dicts(varlist)

    # -- Loop over months
    starttime = datetime.utcnow()
    
    for year in range(yearStart, yearEnd + 1):
        for month in range(monthStart, monthEnd + 1):

            startDate = '%04d%02d%02d' % (year, month, 1)
            numberOfDays = calendar.monthrange(year, month)[1]
            lastDate = '%04d%02d%02d' % (year, month, numberOfDays)
            requestDates = startDate + "/TO/" + lastDate
            target = os.path.join(topdir,"era5_daily_%04d%02d_" % (year, month))

            if clean:
                if an_sfc:
                    tgt = target+'_oper_an_sfc.grb'
                    if os.path.isfile(tgt): os.remove(tgt)
                if an_plev:
                    tgt = target+'_oper_an_pl.grb'
                    if os.path.isfile(tgt): os.remove(tgt)
                if fc_sfc:
                    tgt = target+'_oper_fc_sfc.grb'
                    if os.path.isfile(tgt): os.remove(tgt)
            else:
                # retrieve files and/or postprocess in parallel
                if an_sfc:
                    tgt = target+'_oper_an_sfc.grb'
                    t1 = ExceptionThread(target=era5_request_sfc_oper, args=('an', '/'.join(an_sfc), requestDates, tgt, dwnld, cdo))
                    t1.start()
                if an_plev:
                    tgt = target+'_oper_an_pl.grb'
                    t2 = ExceptionThread(target=era5_request_an_pl_oper, args=('/'.join(an_plev), requestDates, tgt, dwnld, cdo))
                    t2.start()
                if fc_sfc:
                    tgt = target+'_oper_fc_sfc.grb'
                    t3 = ExceptionThread(target=era5_request_sfc_oper, args=('fc', '/'.join(fc_sfc), requestDates, tgt, dwnld, cdo))
                    t3.start()

                for k in threading.enumerate():
                    if k.name != 'MainThread':
                        try:
                            k.join()
                        except Exception as e:
                            print('\n Error raised in {} for month {}-{:02}:'.format(k.name, year, month) )
                            print(e.message )
                            # Re-raise: this will print traceback and return 1 if
                            # called from __main__, other threads will finish, but
                            # the for-loop will stop (this is the point of
                            # ExceptionThread: without it you'd keep looping
                            # despite exceptions):
                            raise
                
    # -- Epilog
    stime = datetime.utcnow() - starttime
    print('\n*II* Processing time {} sec.'.format(stime.total_seconds()) )

    
def build_request_dicts(iwant):
    """
    Returns a list of requested grib codes for each of the three
    STREAM/TYPE/LEVELTYPE considered so far.

    IWANT: (list of string) short names (as in ECMWF GRIB table) or
           Climate Explorer names of the wanted variables.    
    """
    
    # Not available
    shorts = [d['short'] for d in avail]
    cxname = [d['cxname'] for d in avail]
    missing = [d for d in iwant if d not in shorts and d not in cxname]
    if missing: raise Exception('\n\tvariable(s) not available: '+' '.join(missing))
    
    # We can group data request for same stream/type/leveltype.
    # Available (i.e. accounted for in this module) for each group: 
    an_sfc = [d for d in avail if d['type'] == 'an' and d.get('levtype','sfc') == 'sfc']
    an_ple = [d for d in avail if d['type'] == 'an' and d.get('levtype','sfc') == 'pl']
    fc_sfc = [d for d in avail if d['type'] == 'fc']

    # Limit to the wanted one
    was = [d['code'] for d in an_sfc if d['short'] in iwant or d['cxname'] in iwant]
    wap = [d['code'] for d in an_ple if d['short'] in iwant or d['cxname'] in iwant]
    wfs = [d['code'] for d in fc_sfc if d['short'] in iwant or d['cxname'] in iwant]

    return was, wap, wfs
    

def cdo_postproc(params, yyyymmdd, infile):
    """
    All params (and only them) are in the INFILE by design. And they
    all have the same type ('an' or 'fc')
    """
    
    # destination is a subdir
    topdir = os.path.dirname(infile)
    dest = os.path.join(topdir, 'daily', yyyymmdd[0:4])
    makepdir(dest)

    # hack subdir destination
    newdest = os.path.join(hackdst,yyyymmdd[0:4])
    makepdir(newdest)
    
    # -- splitname
    cmd = 'cdo -s -t ecmwf splitname {} {}/era5_{}_'.format(infile, dest, yyyymmdd[0:6])
    printThread( 'split files with "{}"'.format(cmd))
    p = subprocess.Popen( cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout_str,stderr_str = p.communicate()

    if p.returncode:
        print(stderr_str)
        raise RuntimeError('\n\terror while cdo-splitting {}'.format(infile))
    
    # -- cdo+nco
    cdo = "cdo -s -r -R -b 32 -f nc4 -z zip "

    for p in params.split('/'):
        pdes = [d for d in avail if d['code']==p][0]
        printThread(
            "postprocessing parameter {}: {}/{} ({}) type:{}".format(
                p,
                pdes['short'].upper(),
                pdes['cxname'],
                pdes.get('levtype',''), pdes['type']))

        gribf = '{}/era5_{}_{}.grb'.format(dest, yyyymmdd[0:6], pdes['short'].upper() )  # grib file name expected from splitname above

        netcf = gribf.replace( pdes['short'].upper()+'.grb', pdes['cxname']+'.nc'      ) # netCDF file name after cdo
        atted = gribf.replace( pdes['short'].upper()+'.grb', pdes['cxname']+'_atted.nc') # netCDF file name after ncatted
        
        # - convert, scale and/or rename
        if os.path.exists(gribf):

            cmd = ( cdo + " setname," + pdes['cxname'] + " -" + pdes['day_op'] +" "+ gribf +" "+ netcf )

            if pdes.get('factor',1.) != 1.:
                cmd = cmd.replace( 'setname', "mulc," + str(pdes['factor']) + " -setname" )

            if pdes['type'] == 'fc':
                cmd = cmd.replace( gribf, "-shifttime,-1hour " + gribf )

            printThread( 'process file {} with "{}"'.format(gribf,cmd))

            # HACK - keep a copy of some hourly files for Folmer in yearly dirs
            if pdes['short'] in ('t2m','d2m','tp','u10m','v10m'):
                shutil.copy(gribf, newdest)
                
            p = subprocess.Popen( cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout_str, stderr_str = p.communicate()

            if p.returncode:
                print(stdout_str, stderr_str)
                raise RuntimeError('\n\terror with command: {}'.format(cmd))
        else:
            raise NameError(gribf)
        
        # - attributes
        cmd = ['ncatted', '-O',
                '-a', 'long_name,' + pdes['cxname'] + ",a,c," + pdes['long_name'], 
                '-a', 'standard_name,' + pdes['cxname'] + ",a,c," + pdes['standard_name'], 
                '-a', 'units,' + pdes['cxname'] + ",a,c," + pdes['units'], 
                '-a', 'title,global,a,c,"ERA5 reanalysis, https://www.ecmwf.int/en/forecasts/datasets/reanalysis-datasets/era5"',
                netcf, atted]        
        p = subprocess.Popen( cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout_str, stderr_str = p.communicate()
        
        if p.returncode:
            print(stdout_str, stderr_str)
            raise RuntimeError('\n\terror with command: {}'.format(' '.join(cmd)))

        # - clean
        os.rename(atted, netcf)
        os.remove(gribf)


def era5_request_sfc_oper(dtype, params, requestDates, target, dwnld, cdo):
    """
    Retrieve TYPE/SFC/OPER: analysis|forecast / surface / HRES sub-daily values
    dtype        : (string)
    requestDates : (string) YYYYMM01/TO/YYYYMMDD
    target       : (string) output filename
    params       : (string) something like "31.128/34.128/137.128/141.128"
    """

    if dtype == 'an':
        step = "0"
        time = "00:00:00/01:00:00/02:00:00/03:00:00/04:00:00/05:00:00/06:00:00/07:00:00/08:00:00/09:00:00/10:00:00/11:00:00/12:00:00/13:00:00/14:00:00/15:00:00/16:00:00/17:00:00/18:00:00/19:00:00/20:00:00/21:00:00/22:00:00/23:00:00"
    elif dtype == "fc":
        step="1/2/3/4/5/6/7/8/9/10/11/12"
        time="06:00:00/18:00:00"
    else:
        raise RuntimeError('\n\twrong type of data. Should be either fc or an.')
    
    if not os.path.exists(target) and dwnld:
        printThread("About to retrieve {}".format(target))

        try:
            c.retrieve("reanalysis-era5-complete", {
                "class": "ea",
                "dataset": "era5",
                "expver": "1",
                "stream": "oper",
                "type": dtype,
                "date": requestDates,
                "levtype": "sfc",
                "time": time,
                "step": step,
                "param": params,
            }, target)
        except:
            printThread("*EE* error during retrieval, removing target {}".format(target) )
            if os.path.exists(target): os.remove(target)
            raise
    else:
        printThread("*II* do not retrieve data, because {} exists".format(target))


    if cdo:
        if os.path.exists(target): cdo_postproc(params, requestDates, target)
        else: raise NameError(target)


def era5_request_an_pl_oper(params, requestDates, target, dwnld, cdo):
    """"
    Retrieve AN/PL/OPER: analysis/pressure_levels/HRES sub-daily values - Level Hardcoded.
      requestDates : (string) YYYYMM01/TO/YYYYMMDD
      target       : (string) output filename
      params       : (string) something like "129.128/130.128/131.128/132.128/133.128/135.128/157.128"
    Retrieve data on the reduced Gaussian grid that correspond to the spectral truncation.
    """
    
    step="0"
    time="00:00:00/01:00:00/02:00:00/03:00:00/04:00:00/05:00:00/06:00:00/07:00:00/08:00:00/09:00:00/10:00:00/11:00:00/12:00:00/13:00:00/14:00:00/15:00:00/16:00:00/17:00:00/18:00:00/19:00:00/20:00:00/21:00:00/22:00:00/23:00:00"
    
    if not os.path.exists(target) and dwnld:
        printThread("About to retrieve {}".format(target))
        try:
            c.retrieve("reanalysis-era5-complete", {
                "class": "ea",
                "dataset": "era5",
                "date": requestDates,
                "time": time,
                "step": step,                
                "expver": "1",
                "levelist": "500",
                "levtype": "pl",
                "grid":'N320',
                "param": params,
                "stream": "oper",
                "type": "an",
            }, target)
        except:
            printThread("*EE* error during retrieval, removing target {}".format(target) )
            if os.path.exists(target): os.remove(target)
            raise
    else:
        printThread("*II* do not retrieve data, because {} exists".format(target))

    if cdo:
        if os.path.exists(target): cdo_postproc(params, requestDates, target)
        else: raise NameError(target)
            
            
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Retrieve and process ERA5 for climate explorer')

    parser.add_argument("outdir",                help="dir where data are downloaded. Created if needed. Once processed through cdo/nco, the data are found in DIR/daily/YYYY", metavar='DIR')
    parser.add_argument("first_year", type=int,  help="First year to retrieve and/or process")
    parser.add_argument("--vlist",    nargs='*', help="List of variables to process", metavar='VAR')
    parser.add_argument("--last",     type=int,  help="Last year to retrieve and/or process. Default to first_year.")
    parser.add_argument("--mm1",      type=int,  help="first month to process. Default to 1.", metavar='MonthStart', default=1)
    parser.add_argument("--mm2",      type=int,  help="last month to process. Default to 12.", metavar='MonthEnd',   default=12)
    parser.add_argument("--no-dwnld",            help="do not retieve data",                   action="store_true",  default=False)
    parser.add_argument("--no-cdo",              help="do not process retrieval with cdo/nco", action="store_true",  default=False)
    parser.add_argument("--clean",               help="remove downloaded grib files",          action="store_true",  default=False)

    args = parser.parse_args()

    rc = retrieve_daily_era5(
        args.outdir, args.first_year, varlist=args.vlist,
        yearEnd=args.last, monthStart=args.mm1, monthEnd=args.mm2,
        dwnld=not args.no_dwnld, cdo=not args.no_cdo, clean=args.clean)
    
    if rc == 2:
        print('')
        parser.print_help()
        
    sys.exit(rc)

